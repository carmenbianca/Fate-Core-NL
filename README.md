Dit is een super simpele, onvolledige vertaling van Fate Core naar het
Nederlands. Deze vertaling is voldoende om Fate te spelen zonder een enkel woord
Engels uit te hoeven spreken. Je moet echter wel de regels van een Engelstalige
bron leren.

De vertaling bevat:

- Het meest recente personageblad voor Fate Core (laatst gecheckt: Augustus
  2021).
- Het spiekbriefje uit Fate Core.
- De vaardighedenlijst uit Fate Core.
- Een Engels-Nederlands woordenlijst.

De vertaling bevat niet:

- De regels.
